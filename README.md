![输入图片说明](https://images.gitee.com/uploads/images/2021/0105/145619_400a2fe9_6569472.png "微信图片_20210105145527.png")

### 产品介绍

### Niushop社区团购 
 **快速搭建社区团购/社群项目，迅速展开线上线下业务** <br/>
社区团购模式将线上与线下进行结合的团购模式，它是社交电商发展演变出来的一种新模式，主要是依托于真实的社区、利用邻里关系开展的一种团购模式。这种模式非常重视人与人之间的交流，也是需要依靠信任才能够持续下去的商业模式。

### 推荐服务器环境配置

![输入图片说明](https://images.gitee.com/uploads/images/2021/0105/182527_2f976330_6569472.png "服务器配置推荐.png")

### 一、 团长入驻
 团长作为社团系统的引导者，商家在各社区招募便利店主、业主成为团长，团长建立社区微信群，通过邻里关系将周边居民拉入群，在群内分享商品及商城链接，引导群内居民选购下单即可获得佣金，团长入驻流程如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0105/181415_ea48ba4a_6569472.png "团长入驻信息审核中 拷贝 2.png")

### 二、 团长赚取佣金
团长可以通过推广二维码，获得推荐会员，引导他们在自己的自提点下单，赚取一定数额的佣金。在为集体提供便利的同时也获得了额外的收入。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0105/185029_15455b1b_6569472.png "推广.png")

### 三、会员下单流程
会员可选择附近自提点，提交订单，于规定时间内到指定自提点进行扫码提货
![输入图片说明](https://images.gitee.com/uploads/images/2021/0105/184109_00ab1d8f_6569472.png "会员下单流程 5.png")

### 四、社团功能细节介绍
1.会员进入首页浏览时，可直接通过首页的分类菜单，快捷进入到相应的商品分类下直接选择心仪的商品，购物更加方便快捷

![输入图片说明](https://images.gitee.com/uploads/images/2021/0106/113708_1913c5ed_6569472.png "首页导航.png")

2.在首页、分类页可以直接将商品加入购物车，直接点击去结算下单，节省了大部分时间

![输入图片说明](https://images.gitee.com/uploads/images/2021/0107/111843_9f35282b_6569472.png "屏幕截图.png")

3.管理端可自定义装修前台界面风格展示，选择自己喜欢的风格吸引用户眼球

![输入图片说明](https://images.gitee.com/uploads/images/2021/0105/191126_2ffd451b_6569472.png "屏幕截图.png")

4.后台首页概况可直观展示待处理订单、团长待审核、提现申请、近期订单数走向趋势、新增会员数、销售额走向、团长销售排行、商品销售排行等

![输入图片说明](https://images.gitee.com/uploads/images/2021/0106/114154_3c5f39f5_6569472.png "屏幕截图.png")

5.对于产家停产的商品，可以选择从商品库删除，为避免手误操作，删除的商品有一次机会可以在回收站恢复操作找回，如果想彻底删除，那么直接在回收站删除即可。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0106/121114_f93a41e5_6569472.png "商品删除.png")

6.图片每次都得重新上传？各个分类的图片整理耗费大量时间？相册管理帮您彻底解决这个烦恼~

![输入图片说明](https://images.gitee.com/uploads/images/2021/0107/095634_1ec61709_6569472.png "屏幕截图.png")

7.商家可以自行设置当日达、次日达、隔日达，合理安排时间进行配送

![输入图片说明](https://images.gitee.com/uploads/images/2021/0107/095950_37bed3fc_6569472.png "屏幕截图.png")

8.会员下单后，商家在管理端可以在后台看到该订单，当时间达到所设置的发货下单时间后，后台会自动生成配送单，后台可以直接确认配送，或者在配送员用户端，可在自己的配送单中点击确认配送

![输入图片说明](https://images.gitee.com/uploads/images/2021/0107/104915_193daedc_6569472.png "屏幕截图.png")

9.多样化营销模式并存，提高用户下单率

![输入图片说明](https://images.gitee.com/uploads/images/2021/0107/102901_70840aa7_6569472.png "屏幕截图.png")

10.后台可以设置团长社区距离限制，超出该范围的社区将不会展示。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0107/113109_11b7c256_6569472.png "屏幕截图.png")

11.商家可以在管理端对违规的团长进行冻结，或者下架处理，也可在管理端更改团长路线

![输入图片说明](https://images.gitee.com/uploads/images/2021/0107/112551_589b9cbb_6569472.png "屏幕截图.png")



### :tw-1f427: Niushop官方群
 Niushop商城qq开发群1:<a href="https://jq.qq.com/?_wv=1027&k=VrVzi1FI" target="_blank"><img src="//pub.idqqimg.com/wpa/images/group.png" border="0" alt="QQ" /> </a> | qq开发群2:<a href="https://jq.qq.com/?_wv=1027&k=MCtjz6B9" target="_blank"><img src="//pub.idqqimg.com/wpa/images/group.png" border="0" alt="QQ" /> </a> | qq开发群3:<a href="https://jq.qq.com/?_wv=1027&k=H9FLIfTP" target="_blank"><img src="//pub.idqqimg.com/wpa/images/group.png" border="0" alt="QQ" /></a>

### 体验二维码
![输入图片说明](https://images.gitee.com/uploads/images/2021/0105/165151_e35dece1_6569472.png "社区团购.png")

####   :fire:  体验站后台:[<a href='http://cgbdemo.niuteam.cn/shop/index/index.html' target="_blank"> 查看 </a>]       
<a href='http://cgbdemo.niuteam.cn/shop/index/index.html' target="_blank">http://cgbdemo.niuteam.cn/shop/index/index.html</a>  账号：test  密码：123456

### :tw-1f50a: 开源版使用须知

1.仅允许用于个人学习研究使用;

2.开源版不建议商用，如果商用必须保留版权信息，望自觉遵守;

3.禁止将本开源的代码和资源进行任何形式任何名义的出售，否则产生的一切任何后果责任由侵权者自负。


### 合作伙伴
![输入图片说明](https://images.gitee.com/uploads/images/2020/0725/120430_ab7fff0d_6569472.png "画板 1 拷贝 3(4).png")


### 版权信息

版权所有Copyright © 2015-2020 NiuShop开源商城&nbsp;版权所有

All rights reserved。
 
上海牛之云网络科技有限公司&nbsp;提供技术支持  
