<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\event;

use app\model\order\CommunityOrder as CommunityOrderModel;

/**
 * 社区配送单送达完成后
 * Class CommunityDeliveryOrderComplete
 * @package app\event
 */
class CommunityDeliveryOrderComplete
{
    public function handle(array $param)
    {
        // 已送达的配送单数据
        $delivery_data = $param['delivery_data'] ?? [];
        // 商家ID
        $site_id       = $param['site_id'] ?? 0;

        // 更新订单状态
        $community_order_model = new CommunityOrderModel();
        foreach ($delivery_data as $key => $val) {
            $community_order_model->communityReceive($val['delivery_id']);
        }
    }
}
