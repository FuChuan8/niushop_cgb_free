<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com

 * =========================================================
 */
namespace app\event;

use app\model\community\CommunityLevel;

/**
 * 增加默认会员等级
 */
class AddCommunityLevel
{
    /**
     * 事件入口
     * @param $param
     * @return array
     */
    public function handle($param)
    {
        if (!empty($param['site_id'])) {
            $member_level = new CommunityLevel();
            $data         = [
                'site_id'          => $param['site_id'],
                'level_name'       => '白银',
                'commission_rate'       => 10,
            ];
            $res          = $member_level->addCommunityLevel($data);
            return $res;
        }
    }
}
