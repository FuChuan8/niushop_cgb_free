<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com

 * =========================================================
 */
namespace app\event;

use app\model\order\CommunityOrder;

/**
 * 活动类型
 */
class CommunityOrderComplete
{
    /**
     * 活动类型
     * @param $data
     * @return array
     */
    public function handle($data)
    {
        // todo 社区团购订单结算，判断
        $model = new CommunityOrder();
        return $model->orderComplete($data['order_id']);
    }
}
