<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com

 * =========================================================
 */

namespace app\event;

use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\goods\GoodsService as GoodsServiceModel;


/**
 * 增加默认商品相关数据：商品1~3个、商品分类、商品服务
 */
class AddYanshiData
{

    public function handle($param)
    {
        if (!empty($param[ 'site_id' ])) {

            // 商品服务
            $goods_service_data = [
                [
                    'site_id' => $param[ 'site_id' ],
                    'service_name' => '7天无理由退货',
                    'desc' => '支持7天无理由退货(拆封后不支持)'
                ],
                [
                    'site_id' => $param[ 'site_id' ],
                    'service_name' => '闪电退款',
                    'desc' => '闪电退款为会员提供的快速退款服务'
                ],
                [
                    'site_id' => $param[ 'site_id' ],
                    'service_name' => '货到付款',
                    'desc' => '支持送货上门后再收款，支持现金、POS机刷卡等方式'
                ],
                [
                    'site_id' => $param[ 'site_id' ],
                    'service_name' => '运费险',
                    'desc' => '卖家为您购买的商品投保退货运费险（保单生效以确认订单页展示的运费险为准）'
                ],
                [
                    'site_id' => $param[ 'site_id' ],
                    'service_name' => '公益宝贝',
                    'desc' => '购买该商品，每笔成交都会有相应金额捐赠给公益。感谢您的支持，愿公益的快乐伴随您每一天'
                ]
            ];
            $model = new GoodsServiceModel();
            $res = $model->addServiceList($goods_service_data);
            return $res;
        }

    }

}