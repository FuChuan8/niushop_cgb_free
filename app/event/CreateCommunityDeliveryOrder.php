<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * =========================================================
 */

namespace app\event;

/**
 * 生成社区配送单后执行事件
 * Class CronAutoCreateCommunityDeliveryOrder
 * @package app\event
 */
class CreateCommunityDeliveryOrder
{
    public function handle(array $params)
    {
        $delivery_id = $params['delivery_id'] ?? 0;
        $order_id    = $params['order_id'] ?? 0;

        if (!empty($delivery_id)) {

            // 更新订单的配送单信息
            model('order')->update(['delivery_id' => $delivery_id], [['order_id', '=', $order_id]]);
        }
    }
}