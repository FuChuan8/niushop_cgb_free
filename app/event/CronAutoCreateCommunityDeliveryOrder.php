<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * =========================================================
 */

namespace app\event;

use app\model\delivery\Order as DeliveryOrderModel;
use app\model\order\CommunityOrder;
use app\model\order\CommunityOrder as CommunityOrderModel;
use app\model\order\Config as ConfigModel;

/**
 * 自动生成社区配送单
 * Class CronAutoCreateCommunityDeliveryOrder
 * @package app\event
 */
class CronAutoCreateCommunityDeliveryOrder
{
    public function handle($params)
    {
        if (is_array($params) && !empty($params)) {

            $site_id = $params['relate_id'] ?? 0;

            if (!empty($site_id)) {
                $config_model = new ConfigModel();
                $trade_config = $config_model->getTradeConfig($site_id)['data']['value'];
                // 发货的下单时间点
                $book_time = $trade_config['book_time'];
                // 发货的下单当天时间点
                $time = mktime(date('H', $book_time), date('i', $book_time), date('s', $book_time), date('m'), date('d'), date('Y'));
                // 需要生成配送单订单的条件
                $condition = [
                    ['order_type', '=', 5],
                    ['order_status', '=', CommunityOrderModel::ORDER_PAY],
                    ['site_id', '=', $site_id],
                    ['delivery_id', '=', 0],
                    ['cl_id', '>', 0],
                ];
                // 是否执行
                $is_exec = 1;


                if ($book_time > 0) {
                    switch ($trade_config['delivery_type']) {
                        // 当日达 当天执行
                        case 1:
                            $condition = array_merge($condition, [
                                ['create_time', '<=', $time],
                                ['arrive_time', '>=', date_to_time(time_to_date($time, 'Y-m-d'))],
                            ]);
                            break;
                        // 次日达 第二天凌晨执行
                        case 2:
                            $condition = array_merge($condition, [
                                ['create_time', '<=', ($time - (1 * 3600 * 24))],
                                ['arrive_time', '>=', date_to_time(time_to_date(time(), 'Y-m-d'))],
                            ]);
                            break;
                        // 隔日达 第三天凌晨执行
                        case 3:
                            $condition = array_merge($condition, [
                                ['create_time', '<=', ($time - (2 * 3600 * 24))],
                                ['arrive_time', '>=', date_to_time(time_to_date(time(), 'Y-m-d'))],
                            ]);
                            break;
                        default:
                            $is_exec = 0;
                            break;
                    }

                    if ($is_exec) {
                        $community_order_model = new CommunityOrderModel();
                        $delivery_order_model  = new DeliveryOrderModel();

                        // 需要生成配送单的订单
                        $order_data = $community_order_model->getOrderList($condition, 'order_id, site_name, cl_id, arrive_time')['data'];
                        // 批量生成配送单
                        foreach ($order_data as $key => $val) {
                            $delivery_order_model->createOrder(array_merge($val, [
                                'site_id' => $site_id
                            ]));
                        }
                    }
                }
            }
        }
    }
}
