<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace app\event;

use app\model\community\Config as CommunityConfig;
use app\model\delivery\Order as DeliveryOrderModel;
use app\model\order\Config as OrderConfig;

/**
 * 增加社区团购默认配置
 */
class AddCommunityConfig
{
    public function handle(array $params)
    {
        $site_id = $params['site_id'] ?? 0;

        if (!empty($site_id)) {
            // 发货设置
            $order_config_model = new OrderConfig();
            $order_config_model->setTradeConfig([
                // 发货下单时间点
                'book_time'     => mktime(10, 0, 0, date('m'), date('d'), date('Y')),
                // 自提时间点
                'pickup_time'   => mktime(16, 0, 0, date('m'), date('d'), date('Y')),
                // 发货类型 1当日达 2次日达 3隔日达
                'delivery_type' => 1,
            ], $site_id);

            // 增加自动生成配送单事件任务
            $delivery_order_model = new DeliveryOrderModel();
            $delivery_order_model->resetCreateCommunityDeliveryOrderCronExecuteTime($site_id);

            // 团长设置
            $community_model = new CommunityConfig();
            $community_model->setConfig([
                // 团长佣金类型 1: 比例, 2: 金额
                'commission_type'    => 1,
                // 社区距离限制
                'community_distance' => 1000,
                // 允许团长申请
                'is_allow_apply'     => 0
            ], $site_id);

            // 入驻协议
            $community_model->setSettledAgreement([
                // 协议标题
                'title'   => '协议标题',
                // 协议内容
                'content' => '<p><span style="font-size: 12px;">本协议由同意并遵照本协议约定团长服务进行商品/服务推广的法律实体（下称“推广者”、“甲方”或“您”）、技术有限公司（下称“乙方”）</span><span style="font-size: 12px;">，</span><span style="font-size: 12px;">本协议中合称协议方，乙方可单独或合称“我们”。我们向您提供互联网信息服务、与互联网信息服务相关的软件服务、信息审查服务等（在本协议中可单独或合称为“本服务”），其中互联网信息服务由乙方向您提供，与互联网信息服务相关的软件服务及信息审核服务由丙方向您提供。</span><br/></p><p><br/></p><p><strong><span style="font-size: 12px;">一、协议范围及效力</span></strong></p><p><span style="font-size: 12px;">本协议包括协议正文及所有我们已发布或将来发布并依法通知推广者的各类规则(包括但不限于实施细则、解读、产品流程说明、公告等)。所有我们公布的规则均为本协议不可分割的一部分，与本协议正文具有同等法律效力。</span></p><p><br/></p><p><span style="font-size: 12px;">我们可根据国家法律法规变化及维护交易秩序、保护消费者权益需要修改本协议及各类规则，并将通过法定程序以本协议约定的方式通知您。如您不同意变更事项，您有权于变更事项确定的生效日前联系我们反馈意见。如反馈意见得以采纳，我们将酌情调整变更事项。</span></p><p><br/></p><p><span style="font-size: 12px;">如您对已生效的变更事项仍不同意的，您应当于变更事项确定的生效之日起停止使用团长服务，变更事项对您不产生效力；如您在变更事项生效后仍继续使用团长服务，则视为您同意已生效的变更事项。</span></p><p><br/></p><p><span style="font-size: 12px;">除另行声明或签署协议外，我们任何基于提升消费者及推广者体验、平台服务升级等目的所扩大的服务范围或软件功能的增强均受本协议的约束。</span></p><p><br/></p><p><span style="font-size: 12px;">&nbsp;您通过网络页面点击确认本协议或以任何方式实际使用本协议下服务内容，均表示您已同意接受并承诺履行本协议的全部约定内容。本协议自您点击确认之日起成立，并自您满足本协议约定的全部入驻要求、经我们审核通过后并为您开通权限后生效。</span></p><p><br/></p><p><strong><span style="font-size: 12px;">&nbsp;二、账户使用及信息提交</span></strong></p><p><span style="font-size: 12px;">您在使用服务的过程中须登录账户关联您团长的商业信息并可能涉及消费者利益，未经我们同意，您不得向他人转让或授权他人使用，否则我们可终止对该推广者账户提供服务。</span></p><p><br/></p><p><span style="font-size: 12px;">除我们存在过错外，您应对您账户项下的所有行为结果（如在线签署协议、发布及披露信息等）负责。通过您的账户完成的行为均视为您本人行为或已获得您的充分授权。</span></p><p><br/></p><p><span style="font-size: 12px;">您了解并同意，如基于法律法规要求或为实现本协议项下服务的目的，我们要求您填写或提供相关信息的，您有义务确保您提供的信息及资料真实、合法、准确、有效，如有发生变更须及时更新。基于相关法律法规、平台管理等要求，推广者同意我们可对推广者信息和证明文件进行不定时抽查并就前述目的要求推广者提交更多信息或证明文件。您了解并认可我们仅以大众一般认知水平对推广者信息、证明文件及推广权限开通申请进行审查，除法律另有规定外，我们对推广者因信赖审查结果而造成的损失不承担责任。</span><br/></p><p><br/></p><p><strong><span style="font-size: 12px;">三、推广者管理及服务规范</span></strong></p><p><span style="font-size: 12px;">您须年满18周岁，具备合法的权利能力和行为能力缔结本协议并使用我们的相关服务，且您有义务依据相关法律法规、协议、规则等向我们提供真实、合法、准确、有效的信息和资料，包括进行实名/实人认证、提供联系方式等。为了您能正常使用团长功能，为下单的买家提供温馨安全的服务，我们会审核您提交的信息，审查结果以我们的评估判断为准。</span><br/></p><p><span style="font-size: 12px;"><br/></span></p><p><span style="font-size: 12px;">您承诺有权对您提供的提货点享有合法、完整的所有权、使用权和经营权，可在提货点内为推广商品提供存储、保管、分发及自提等服务，并承担因此产生的所有费用。如因您的原因导致买家、卖家、我们或其他第三方合法权益受损，您须自行承担由此引发的相应责任及后果，法律另有规定的除外, 您理解并确认，您在推广服务过程中须遵守规则中的相关规定。</span><br/></p><p><br/></p><p><strong><span style="font-size: 12px;">四、争议解决及其他</span></strong></p><p><span style="font-size: 12px;">我们可根据业务调整情况将本协议项下的全部或部分权利，我们承诺此变更不会影响推广者在此协议下的权益或加重推广者的义务。</span></p><p><br/></p><p><span style="font-size: 12px;">本协议之解释与适用，以及与本协议有关的争议，均应依照中华人民共和国法律。由本协议引起的及与本协议有关的争议，以丙方住所地有管辖权的人民法院为第一审管辖法院。</span></p><p><br/></p><p><span style="font-size: 12px;">如本协议的任何条款被视作无效或无法执行，则上述无效或无法执行条款可被分离且不影响本协议其余条款的有效性及可执行性。</span></p><p><br/></p><p><span style="font-size: 12px;">我们于推广者过失或违约时放弃本协议规定的权利，不应视为其对推广者其他或以后同类之过失或违约行为弃权。</span></p>'
            ], $site_id);
        }
    }
}