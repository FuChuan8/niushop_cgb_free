<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com

 * =========================================================
 */

namespace addon\seckill\api\controller;

use addon\seckill\model\Seckill as SeckillModel;
use app\api\controller\BaseApi;


/**
 * 秒杀
 */
class Seckill extends BaseApi
{

    /**
     * 列表信息
     */
    public function lists()
    {

        $today_time = strtotime(date("Y-m-d"), time());
        $time = time() - $today_time;//当日时间戳

        $condition = [
            ['site_id','=',$this->site_id],
            ['seckill_end_time', '>=', $time]
        ];
        $order     = 'seckill_start_time asc';
        $field     = 'id,name,seckill_start_time,seckill_end_time';

        $seckill_model = new SeckillModel();
        $list          = $seckill_model->getGoodsSeckillTimeList($condition, $field, $order, null);
        $list          = $list['data'];
        foreach ($list as $key => $val) {
            $val                                   = $seckill_model->transformSeckillTime($val);
            $list[$key]['seckill_start_time_show'] = "{$val['start_hour']}:{$val['start_minute']}:{$val['start_second']}";
            $list[$key]['seckill_end_time_show']   = "{$val['end_hour']}:{$val['end_minute']}:{$val['end_second']}";
        }
        $res = [
            'list' => $list
        ];
        return $this->response($this->success($res));
    }
}