<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com

 * =========================================================
 */

namespace addon\seckill\api\controller;

use app\api\controller\BaseApi;
use addon\seckill\model\Seckill as SeckillModel;
use addon\seckill\model\Poster;
use addon\seckill\model\SeckillOrderCreate;
use app\model\goods\GoodsService;


/**
 * 秒杀商品
 */
class Seckillgoods extends BaseApi
{

    /**
     * 详情信息
     */
    public function detail()
    {

        $seckill_id = isset($this->params[ 'seckill_id' ]) ? $this->params[ 'seckill_id' ] : 0;
        if (empty($seckill_id)) {
            return $this->response($this->error('', 'REQUEST_ID'));
        }
        $seckill_model = new SeckillModel();
        $order = new SeckillOrderCreate();
        $condition = [
            [ 'ps.id', '=', $seckill_id ],
            [ 'psg.site_id', '=', $this->site_id ],
            [ 'psg.status', '=', 1 ],
            [ 'ps.status', '=', 1 ],
            [ 'g.goods_state', '=', 1 ],
            [ 'g.is_delete', '=', 0 ]
        ];
        $goods_sku_detail = $seckill_model->getSeckillGoodsInfo($condition);
        $goods_sku_detail = $goods_sku_detail[ 'data' ];

        $num = $order->getGoodsSeckillNum($seckill_id);
        $time_data = $seckill_model->getSeckillInfo($seckill_id);
        $time_data = $time_data[ 'data' ];
        $goods_sku_detail[ 'sale_num' ] = $num;
        $goods_sku_detail[ 'seckill_start_time' ] = $time_data[ 'seckill_start_time' ];
        $goods_sku_detail[ 'seckill_end_time' ] = $time_data[ 'seckill_end_time' ];

        $res[ 'goods_sku_detail' ] = $goods_sku_detail;
        if (empty($goods_sku_detail)) return $this->response($this->error($res));

        if (!empty($goods_sku_detail[ 'goods_spec_format' ])) {
            //判断商品规格项
            $goods_spec_format = $seckill_model->getGoodsSpecFormat($seckill_id, $this->site_id, $goods_sku_detail[ 'goods_spec_format' ]);
            $res[ 'goods_sku_detail' ][ 'goods_spec_format' ] = json_encode($goods_spec_format);
        }

        $goods_service = new GoodsService();
        $goods_service_list = $goods_service->getServiceList([ [ 'site_id', '=', $this->site_id ], [ 'id', 'in', $res[ 'goods_sku_detail' ][ 'goods_service_ids' ] ] ], 'service_name,desc');
        $res[ 'goods_sku_detail' ][ 'goods_service' ] = $goods_service_list[ 'data' ];

        return $this->response($this->success($res));
    }


    /**
     * 基础信息
     */
    public function info()
    {
        $seckill_id = isset($this->params[ 'seckill_id' ]) ? $this->params[ 'seckill_id' ] : 0;
        $sku_id = isset($this->params[ 'sku_id' ]) ? $this->params[ 'sku_id' ] : 0;
        if (empty($seckill_id)) {
            return $this->response($this->error('', 'REQUEST_ID'));
        }
        if (empty($sku_id)) {
            return $this->response($this->error('', 'REQUEST_ID'));
        }
        $seckill_model = new SeckillModel();
        $order = new SeckillOrderCreate();
        $condition = [
            [ 'ps.id', '=', $seckill_id ],
            [ 'psg.sku_id', '=', $sku_id ],
            [ 'psg.site_id', '=', $this->site_id ],
            [ 'psg.status', '=', 1 ],
            [ 'ps.status', '=', 1 ],
            [ 'g.goods_state', '=', 1 ],
            [ 'g.is_delete', '=', 0 ]
        ];
        $goods_sku_detail = $seckill_model->getSeckillGoodsInfo($condition);
        $goods_sku_detail = $goods_sku_detail[ 'data' ];

        $res[ 'goods_sku_detail' ] = $goods_sku_detail;
        if (!empty($goods_sku_detail)) {

            $num = $order->getGoodsSeckillNum($seckill_id);
            $time_data = $seckill_model->getSeckillInfo($seckill_id);
            $time_data = $time_data[ 'data' ];
            $goods_sku_detail[ 'sale_num' ] = $num;
            $goods_sku_detail[ 'seckill_start_time' ] = $time_data[ 'seckill_start_time' ];
            $goods_sku_detail[ 'seckill_end_time' ] = $time_data[ 'seckill_end_time' ];
            //判断商品规格项
            $goods_spec_format = $seckill_model->getGoodsSpecFormat($seckill_id, $this->site_id, $goods_sku_detail[ 'goods_spec_format' ]);
            $res[ 'goods_sku_detail' ][ 'goods_spec_format' ] = json_encode($goods_spec_format);

        } else {
            $sku_id = $seckill_model->getGoodsSpecFormat($seckill_id, $this->site_id,'', $sku_id);
            $res = [ 'type' => 'again', 'sku_id' => $sku_id ];
        }
        return $this->response($this->success($res));
    }

    public function page()
    {
        $seckill_time_id = isset($this->params[ 'seckill_time_id' ]) ? $this->params[ 'seckill_time_id' ] : 0;
        $page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
        $page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;

        if (empty($seckill_time_id)) {
            return $this->response($this->error('', 'REQUEST_SECKILL_ID'));
        }

        $seckill_model = new SeckillModel();
        $res = $seckill_model->getSeckillGoodsPageList($seckill_time_id, $this->site_id, $page, $page_size);
        $list = $res[ 'data' ][ 'list' ];
        foreach ($list as $key => $val) {
            if ($val[ 'price' ] != 0) {
                $discount_rate = floor($val[ 'seckill_price' ] / $val[ 'price' ] * 100);
            } else {
                $discount_rate = 100;
            }
            $list[$key]['discount_rate'] = $discount_rate;//折扣比率
            $list[$key]['activity_type'] = 'seckill';//活动类型
            $list[$key]['activity_id']   = $val['seckill_id'];//活动ID
            $list[$key]['min_buy']       = 0;//活动ID
        }
        $res = [
            'list' => $list
        ];
        return $this->response($this->success($res));
    }

    /**
     * 获取商品海报
     */
    public function poster()
    {
        if (!empty($qrcode_param)) return $this->response($this->error('', '缺少必须参数qrcode_param'));

        $promotion_type = 'seckill';
        $qrcode_param = json_decode($this->params[ 'qrcode_param' ], true);
        $qrcode_param[ 'source_member' ] = $qrcode_param[ 'source_member' ] ?? 0;
        $poster = new Poster();
        $res = $poster->goods($this->params[ 'app_type' ], $this->params[ 'page' ], $qrcode_param, $promotion_type, $this->site_id);
        return $this->response($res);
    }

}