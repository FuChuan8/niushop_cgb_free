<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+
return [
    'name' => 'seckill',
    'title' => '限时秒杀',
    'description' => '限时秒杀功能',
    'type' => 'promotion', //插件类型  system :系统插件    promotion:营销插件  tool:工具插件
    'status' => 1,
    'author' => '',
    'version' => '1.0.0',
    'version_no' => '202101040001',
    'content' => '',
];