<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\seckill\shop\controller;

use app\shop\controller\BaseShop;
use addon\seckill\model\Seckill as SeckillModel;

/**
 * 秒杀控制器
 */
class Seckill extends BaseShop
{
    /**
     * 秒杀时间段列表
     */
    public function lists()
    {
        if (request()->isAjax()) {
            $condition[] = ['site_id', '=', $this->site_id];
            $order       = 'seckill_start_time asc';
            $field       = '*';

            $seckill_model = new SeckillModel();
            $res           = $seckill_model->getSeckillTimeList($condition, $field, $order, null);
            foreach ($res['data'] as $key => $val) {
                $val                                          = $seckill_model->transformSeckillTime($val);
                $res['data'][$key]['seckill_start_time_show'] = "{$val['start_hour']}:{$val['start_minute']}:{$val['start_second']}";
                $res['data'][$key]['seckill_end_time_show']   = "{$val['end_hour']}:{$val['end_minute']}:{$val['end_second']}";
            }
            return $res;
        } else {
            $this->forthMenu();
            return $this->fetch("seckill/lists");
        }
    }

    /**
     * 添加秒杀时间段
     */
    public function add()
    {
        if (request()->isAjax()) {
            $start_hour   = input('start_hour', 0);
            $start_minute = input('start_minute', 0);
            $start_second = input('start_second', 0);

            $end_hour   = input('end_hour', 0);
            $end_minute = input('end_minute', 0);
            $end_second = input('end_second', 0);

            $data          = [
                'site_id'            => $this->site_id,
                'name'               => input('name', ''),
                'seckill_start_time' => $start_hour * 3600 + $start_minute * 60 + $start_second,
                'seckill_end_time'   => $end_hour * 3600 + $end_minute * 60 + $end_second,
                'create_time'        => time(),
            ];
            $seckill_model = new SeckillModel();
            return $seckill_model->addSeckillTime($data);
        } else {
            return $this->fetch("seckill/add");
        }
    }

    /**
     * 编辑秒杀时间段
     */
    public function edit()
    {
        $seckill_model = new SeckillModel();
        if (request()->isAjax()) {
            $start_hour   = input('start_hour', 0);
            $start_minute = input('start_minute', 0);
            $start_second = input('start_second', 0);

            $end_hour   = input('end_hour', 0);
            $end_minute = input('end_minute', 0);
            $end_second = input('end_second', 0);

            $data = [
                'name'               => input('name', ''),
                'seckill_start_time' => $start_hour * 3600 + $start_minute * 60 + $start_second,
                'seckill_end_time'   => $end_hour * 3600 + $end_minute * 60 + $end_second,
                'create_time'        => time(),
                'id'                 => input('id', 0),
            ];
            return $seckill_model->editSeckillTime($data, $this->site_id);
        } else {
            $id = input('id', 0);
            $this->assign('id', $id);

            //秒杀详情
            $time_info = $seckill_model->getSeckillTimeInfo([['id', '=', $id]]);
            if (!empty($time_info['data'])) {
                $time_info['data'] = $seckill_model->transformSeckillTime($time_info['data']);
            }
            $this->assign('time_info', $time_info['data']);

            return $this->fetch("seckill/edit");
        }
    }

    /**
     * 删除秒杀时间段
     */
    public function delete()
    {
        if (request()->isAjax()) {
            $seckill_time_id    = input('id', 0);
            $seckill_model = new SeckillModel();
            return $seckill_model->deleteSeckillTime($seckill_time_id);
        }
    }


    /**
     * 添加秒杀商品
     */
    public function addGoods()
    {
        $seckill_model = new SeckillModel();
        if (request()->isAjax()) {
            $data = [
                'seckill_name'   => input('seckill_name', ''),
                'remark'         => input('remark', ''),
                'seckill_time_id'=> input('seckill_time_id', ''),
                'start_time'     => strtotime(input('start_time', '')),
                'end_time'       => strtotime(input('end_time', '')),
                'site_id'        => $this->site_id,
                'goods_data'     => input('goods_data', ''),
                'goods_ids'      => input('goods_ids', '')
            ];
            $res = $seckill_model->addSeckillGoods($data);
            return $res;
        } else {
            $seckill_time_list = $seckill_model->getSeckillTimeList([ ['site_id', '=', $this->site_id] ]);
            foreach ($seckill_time_list['data'] as $key => $val) {
                $val = $seckill_model->transformSeckillTime($val);
                $seckill_time_list['data'][$key]['seckill_start_time_show'] = "{$val['start_hour']}:{$val['start_minute']}:{$val['start_second']}";
                $seckill_time_list['data'][$key]['seckill_end_time_show']   = "{$val['end_hour']}:{$val['end_minute']}:{$val['end_second']}";
            }
            $this->assign('seckill_time_list', $seckill_time_list);

            return $this->fetch("seckill/addgoods");
        }
    }


    /**
     * 更新商品（秒杀价格）
     */
    public function updateGoods()
    {
        $seckill_model = new SeckillModel();
        if (request()->isAjax()) {
            $data = [
                'seckill_name'   => input('seckill_name', ''),
                'remark'         => input('remark', ''),
                'seckill_time_id'=> input('seckill_time_id', ''),
                'start_time'     => strtotime(input('start_time', '')),
                'end_time'       => strtotime(input('end_time', '')),
                'site_id'        => $this->site_id,
                'sku_list'       => input('sku_list', ''),
                'goods_ids'      => input('goods_ids', ''),
                'id'             => input('id', '')
            ];
            $res = $seckill_model->editSeckillGoods($data);
            return $res;
        } else {
            $seckill_id = input('id', '');
            $seckill_info = $seckill_model->getSeckillDetail([ ['id','=',$seckill_id] ]);
            $time_list = $seckill_model->getSeckillTimeList( [ ['id','in',$seckill_info['data']['seckill_time_id']] ] );

            $this->assign('seckill_info', $seckill_info['data']);
            $this->assign('time_list', $time_list['data']);
            return $this->fetch("seckill/editgoods");
        }
    }


    /**
     * 删除商品
     */
    public function deleteGoods()
    {
        if (request()->isAjax()) {
            $seckill_id = input('id', 0);
            $site_id    = $this->site_id;

            $seckill_model = new SeckillModel();
            return $seckill_model->deleteSeckillGoods($seckill_id, $site_id);
        }
    }

    /**
     * 秒杀商品
     */
    public function goodslist()
    {
        if (request()->isAjax()) {
            $page      = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $goods_name  = input('goods_name', '');
            $status  = input('status', '');

            $condition   = [];
            $condition[] = ['site_id', '=', $this->site_id];
            $condition[] = ['goods_name', 'like', '%' . $goods_name . '%'];

            if($status !== '') $condition[] = ['status', '=', $status];

            $seckill_model = new SeckillModel();
            $seckill_list = $seckill_model->getSeckillPageList($condition, $page, $page_size, 'id desc');

            $time_list = $seckill_model->getSeckillTimeList([ ['site_id','=',$this->site_id] ]);

            foreach ($seckill_list['data']['list'] as $k => $v){
                $seckill_list['data']['list'][$k]['time_list'] = [];
                foreach ($time_list['data'] as $index => $item){
                    if(strpos(','.$v['seckill_time_id'].',', ','.$item['id'].',') !== false ){
                        $seckill_list['data']['list'][$k]['time_list'][] = $item;
                    }
                }
            }

            return $seckill_list;

        } else {
            return $this->fetch("seckill/goodslist");
        }
    }

    /**
     * 秒杀时段
     */
    public function seckilltimeselect(){

        if (request()->isAjax()) {
            $condition[] = ['site_id', '=', $this->site_id];
            $order       = 'seckill_start_time asc';
            $field       = '*';

            $seckill_model = new SeckillModel();
            $res           = $seckill_model->getSeckillTimeList($condition, $field, $order, null);
            foreach ($res['data'] as $key => $val) {
                $val                                          = $seckill_model->transformSeckillTime($val);
                $res['data'][$key]['seckill_start_time_show'] = "{$val['start_hour']}:{$val['start_minute']}:{$val['start_second']}";
                $res['data'][$key]['seckill_end_time_show']   = "{$val['end_hour']}:{$val['end_minute']}:{$val['end_second']}";
            }
            return $res;
        } else {
            $this->forthMenu();
            return $this->fetch("seckill/seckilltimeselect");
        }

    }


    /**
     * 获取商品列表
     * @return array
     */
    public function getSkuList()
    {
        if(request()->isAjax()){
            $seckill_model = new SeckillModel();

            $seckill_id = input('seckill_id', '');

            $goods_list = $seckill_model->getSeckillGoodsList($seckill_id);
            return $goods_list;
        }
    }
    /**
     * 手动关闭秒杀
     * @return array
     */
    public function closeSeckill()
    {
        if(request()->isAjax()){
            $seckill_model = new SeckillModel();

            $seckill_id = input('seckill_id', '');

            $goods_list = $seckill_model->closeSeckill($seckill_id);
            return $goods_list;
        }
    }


}